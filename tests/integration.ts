import { deployIdentityContract, getStaxConfiguredRegistry } from "../ts";
import { JolocomLib, claimsMetadata } from "jolocom-lib";
import { KeyTypes } from "jolocom-lib/js/vaultedKeyProvider/types";
import {
  addHexPrefixIfNotPresent,
  awaitPaymentTxConfirmation,
  fuelAddress,
  getAddressFromPrivKey,
  getStaxEndpoints
} from "../ts/utils/utils";
import {http} from '../ts/utils/http'
import { randomBytes } from 'crypto'

const staxEndpoint = "https://r2bapi.dltstax.net";
const privateKey = `${"6".repeat(64)}`;
const TEST_SEED = randomBytes(32)
const TEST_PASS = "secret";

async function testPayment(iw, paymentEndpoint) {
  console.log('Creating test payment request')
  const paymentRequest = await iw.create.interactionTokens.request.payment({
    callbackURL: 'https://example.com/payment',
    description: 'Testing',
    transactionOptions: {
      value: 6666666666666,
      to: '0x' + '0'.repeat(40)
    }
  }, TEST_PASS)

  const txHash = await iw.transactions.sendTransaction(paymentRequest.interactionToken, TEST_PASS)
  return await awaitPaymentTxConfirmation(paymentEndpoint, txHash, http);
}

async function testCredentialValidation(iw, reg) {
  console.log('Creating credential validation')
  const cred = await iw.create.signedCredential(
    {
      metadata: claimsMetadata.emailAddress,
      claim: {
        email: 'e@g.c'
      },
      subject: iw.did
    },
    TEST_PASS
  )
  console.log(`Checking credential validity`)
  console.log(`Valid - ${await JolocomLib.util.validateDigestable(cred, reg)}`)
}

deployIdentityContract(staxEndpoint, privateKey, http).then(async res => {
  const reg = getStaxConfiguredRegistry(
    "https://r2bapi.dltstax.net",
    addHexPrefixIfNotPresent(res.address),
    http,
    res.abi
  );

  const { userInfoEndpoint, paymentEndpoint } = getStaxEndpoints(staxEndpoint);
  const vaultedKeyProvider = new JolocomLib.KeyProvider(TEST_SEED, TEST_PASS);

  const priv = vaultedKeyProvider.getPrivateKey({
    derivationPath: KeyTypes.ethereumKey,
    encryptionPass: TEST_PASS
  });

  console.log(
    `Fueling address ${getAddressFromPrivKey(priv)} for identity creation`
  );

  const ref_id = await fuelAddress(
    userInfoEndpoint,
    getAddressFromPrivKey(priv),
    http
  );

  await awaitPaymentTxConfirmation(paymentEndpoint, ref_id, http);
  const iw = await reg.create(vaultedKeyProvider, TEST_PASS);
  await testPayment(iw, paymentEndpoint)
  await testCredentialValidation(iw, reg)
});
