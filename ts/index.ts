import { JolocomLib } from "jolocom-lib"
import {
  getStaxConfiguredStorageConnector,
  getStaxConfiguredContractsConnector,
  getStaxConfiguredContractsGateway
} from 'jolocom-lib-stax-connector'
import {
  awaitContractTxConfirmation,
  awaitPaymentTxConfirmation,
  base64Encode,
  fuelAddress,
  getAddressFromPrivKey,
  getStaxEndpoints,
  getValidNonceForAddress
} from "./utils/utils"
import {ContractArtifact, ContractMetadata} from './types'
import {
  assembleTx,
  computeContractAddress,
  getDeploymentTxData,
  sendRawTx
} from './utils/contracts'
import {http} from './utils/http'
import {ContractsAdapter} from 'jolocom-lib/js/contracts/contractsAdapter'
import {HttpAgent} from 'jolocom-lib-stax-connector/js/types'

const registryContract = require("../contract/Registry.json");

export function getStaxConfiguredRegistry(
  baseUrl: string,
  contractAddress: string,
  httpAgent: HttpAgent,
  contractAbi: any[] = registryContract.abi
) {
  return JolocomLib.registries.jolocom.create({
    ipfsConnector: getStaxConfiguredStorageConnector(baseUrl, http),
    ethereumConnector: getStaxConfiguredContractsConnector(
      baseUrl,
      contractAddress,
      httpAgent,
      contractAbi
    ),
    contracts: {
      adapter: new ContractsAdapter(777),
      gateway: getStaxConfiguredContractsGateway(
        baseUrl,
        777,
        httpAgent
      )
    }
  })
}

export async function deployIdentityContract(
  baseUrl: string,
  privateKey: string,
  httpAgent: HttpAgent,
  contract: ContractArtifact = registryContract
): Promise<ContractMetadata> {
  const {
    userInfoEndpoint,
    rawTxEndpoint,
    baseContractsEndpoint,
    paymentEndpoint
  } = getStaxEndpoints(baseUrl);
  const userAddress = getAddressFromPrivKey(privateKey);

  console.log(`Using ${userAddress} to deploy identity contract`);
  console.log("Fueling with Ether");

  const refId = await fuelAddress(userInfoEndpoint, userAddress, httpAgent);

  await awaitPaymentTxConfirmation(paymentEndpoint, refId, httpAgent);

  console.log("Fetching valid nonce");
  const validNonce = await getValidNonceForAddress(
    userInfoEndpoint,
    userAddress,
    httpAgent
  );
  console.log(`Using nonce - ${validNonce}`);

  const transaction = assembleTx(
    getDeploymentTxData(contract),
    userAddress,
    validNonce
  );

  transaction.sign(Buffer.from(privateKey, "hex"));
  const serializedTx = transaction.serialize().toString("hex");
  console.log(`Deployment transaction assembled, broadcasting`);

  return sendRawTx(rawTxEndpoint, serializedTx, httpAgent).then(async refId => {
    await awaitContractTxConfirmation(baseContractsEndpoint, refId, httpAgent);
    const address = computeContractAddress(userAddress, validNonce);
    console.log(`Contract address - ${address}`);

    return {
      txHash: refId,
      address,
      abi: contract.abi,
      base64EncodedAbi: base64Encode(JSON.stringify(contract.abi))
    };
  });
}
