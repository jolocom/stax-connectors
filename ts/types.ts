/**
 * @dev Currently using dev setup that does not implement workspaces / contexts.
 */

export type EndpointMap = {
  paymentEndpoint: string,
  rawTxEndpoint: string,
  userInfoEndpoint: string,
  storageEndpoint: string,
  baseContractsEndpoint: string
}

export type ContractArtifact = {
  abi: any[],
  bytecode: string
}

export type AccountInfo = {
  nonceLatest: number
  noncePending: number
  balance: number
}

export type ContractMetadata = {
  address: string;
  abi: ContractArtifact["abi"];
  base64EncodedAbi?: string;
};

