import { ContractArtifact } from "../types";
import { ethers } from "ethers";
import { hexlify } from "ethers/utils";
import {HttpAgent} from 'jolocom-lib-stax-connector/js/types'

const Transaction = require("ethereumjs-tx");
const RLP = require("rlp");
const createKeccakHash = require("keccak");

export const computeContractAddress = (address: string, nonce: number) =>
  createKeccakHash("keccak256")
    .update(RLP.encode([address, nonce]))
    .digest("hex")
    .substring(24);

export const getDeploymentTxData = ({ abi, bytecode }: ContractArtifact) =>
  hexlify(
    new ethers.ContractFactory(abi, bytecode).getDeployTransaction().data
  );

export function assembleTx(
  data: string,
  from: string,
  nonce: number,
  gas: number = 5e5
) {
  return new Transaction({
    nonce: nonce,
    gas,
    chainID: 777,
    data
  });
}

export async function sendRawTx(endpoint: string, txData: string, httpAgent: HttpAgent) {
  const _binaryPostHeaders = {
    accept: "application/binary",
    "content-type": "application/json;charset=UTF-8"
  };

  return httpAgent.postRequest<{ ref_id: string }>(
    endpoint,
    _binaryPostHeaders,
    txData
  ).then(res => res.ref_id);
}
