import { ethers } from "ethers";
import { AccountInfo, EndpointMap } from "../types";
import { HttpAgent } from 'jolocom-lib-stax-connector/js/types'

/** @description Helper function to generate all staX endpoints from a deployment URL
 */

export const getStaxEndpoints = (baseUrl: string): EndpointMap => ({
  paymentEndpoint: `${baseUrl}/payment`,
  rawTxEndpoint: `${baseUrl}/contracts/raw`,
  userInfoEndpoint: `${baseUrl}/contracts/fueling`,
  storageEndpoint: `${baseUrl}/storage`,
  baseContractsEndpoint: `${baseUrl}/contracts`
});

export async function fuelAddress(
  endpoint: string,
  address: string,
  httpAgent: HttpAgent,
  amount: number = 10e18
) {
  return httpAgent.postRequest<{ ref_id: string }>(
    `${endpoint}/${address}`,
    {},
    { amount }
  ).then(res => res.ref_id);
}

export async function getAddressInfo(endpoint: string, address: string, httpAgent: HttpAgent) {
  return httpAgent.getRequest<AccountInfo>(`${endpoint}/${address}`)
}

export const getAddressFromPrivKey = (privKey: string) => {
  return new ethers.Wallet(privKey, ethers.getDefaultProvider()).address;
};

export async function getValidNonceForAddress(
  endpoint: string,
  address: string,
  httpAgent: HttpAgent
) {
  return getAddressInfo(endpoint, address, httpAgent).then(res => res.noncePending);
}

export function addHexPrefixIfNotPresent(data: string) {
  if (data.indexOf("0x") === 0) {
    return data;
  }

  return "0x" + data;
}

export function stripDidPrefix(did: string): string {
  if (did.indexOf("did:") === 0) {
    return did.substring(did.lastIndexOf(":") + 1);
  }

  console.error(`Malformed DID - ${did} passed to _stripDidPrefix`);
  return did;
}

export function base64Encode(toEncode: string) {
  return Buffer.from(toEncode).toString("base64");
}

export async function awaitContractTxConfirmation(
  endpoint: string,
  txHash: string,
  httpAgent: HttpAgent
) {
  console.log(`Waiting for contract tx - ${txHash} to be mined`)
  return awaitConfirmation(`${endpoint}?txHash=${txHash}`, httpAgent);
}

export async function awaitPaymentTxConfirmation(
  endpoint: string,
  txHash: string,
  httpAgent: HttpAgent
) {
  return awaitConfirmation(`${endpoint}/${txHash}`, httpAgent);
}

export async function awaitConfirmation(
  endpoint: string,
  httpAgent: HttpAgent,
  interval: number = 3000,
): Promise<boolean> {

  await sleep(interval);
  const deployed = await httpAgent.headRequest(endpoint).then(
    res => res.status === 200
  );

  console.log(`Status for ...${endpoint.slice(-8)} - ${deployed ? "Mined!" : "Waiting..."}`);
  return deployed || awaitConfirmation(endpoint, httpAgent, interval);
}

async function sleep(timeInMs: number) {
  await new Promise(resolve => setTimeout(resolve, timeInMs));
}
