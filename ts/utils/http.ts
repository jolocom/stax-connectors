import axios from "axios";
import { HttpAgent } from "jolocom-lib-stax-connector/js/types"

export const http: HttpAgent = {
  getRequest: endpoint => {
    return axios.get(endpoint).then(res => res.data);
  },

  postRequest: (endpoint, headers, data) => {
    return axios.post(endpoint, data, { headers }).then(res => res.data);
  },

  headRequest: endpoint => {
    return axios.head(endpoint)
      .then(res => res)
      .catch(err => err);
  }
};
