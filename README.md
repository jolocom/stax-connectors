Connectors allowing [JolocomLib](https://github.com/jolocom/jolocom-lib) to interface with a deployed staX instance.
Methods for deploying the Jolocom identity contract, and adaptors for anchoring / resolving
identities are provided.

Please check the `./tests/integration.ts` file for usage examples.